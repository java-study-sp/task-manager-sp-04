package com.morozov.tm.rest.project;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProjectUpdateRestTest {
    @NotNull private static final String PROJECT_CREATE_REST_URL = "http://localhost:8080/restprojects/create";
    @NotNull private static final String PROJECT_DELETE_REST_URL = "http://localhost:8080/restprojects/delete";
    @NotNull private static final String PROJECT_UPDATE_REST_URL = "http://localhost:8080/restprojects/update";
    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();
    @Nullable private String projectId;

    @Before
    public void setUp() throws Exception {
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        @NotNull final String projectName = "Test Project";
        requestBody.add("projectName", projectName);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final String projectResultAsJson = restTemplate.postForObject(PROJECT_CREATE_REST_URL, request, String.class);
        @NotNull final JsonNode root = objectMapper.readTree(projectResultAsJson);
        projectId = root.path("id").asText();
    }

    @Test
    public void updateProject() throws IOException {
        @NotNull final HttpHeaders header = new HttpHeaders();
        @NotNull final HttpHeaders headerUpdate = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBodyUpdate = new LinkedMultiValueMap<>();
        requestBodyUpdate.add("id", projectId);
        @NotNull final String updateProjectName = "Test Project update";
        requestBodyUpdate.add("name", updateProjectName);
        requestBodyUpdate.add("description", "");
        requestBodyUpdate.add("dataBegin", "");
        requestBodyUpdate.add("dataEnd", "");
        @NotNull final HttpEntity<?> requestUpdate = new HttpEntity<Object>(requestBodyUpdate, headerUpdate);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final String projectUpdateResultAsJson = restTemplate.postForObject(PROJECT_UPDATE_REST_URL, requestUpdate, String.class);
        @NotNull final JsonNode root = objectMapper.readTree(projectUpdateResultAsJson);
        assertNotNull(projectUpdateResultAsJson);
        assertNotNull(root);
        assertNotNull(root.path("name").asText());
        assertEquals(root.path("name").asText(), updateProjectName);
        assertNotNull(root.path("id").asText());
        assertEquals(root.path("id").asText(), projectId);
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("id", projectId);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(PROJECT_DELETE_REST_URL, request, String.class);
    }
}
