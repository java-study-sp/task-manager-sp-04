package com.morozov.tm.rest.project;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProjectFindByIdRestTest {
    @NotNull private static final String PROJECT_CREATE_REST_URL = "http://localhost:8080/restprojects/create";
    @NotNull private static final String PROJECT_DELETE_REST_URL = "http://localhost:8080/restprojects/delete";
    @NotNull private static final String PROJECT_FIND_BY_ID_REST_URL = "http://localhost:8080/restprojects/find/";
    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();
    @NotNull private final String projectName = "Test Project";
    @Nullable private String projectId;
    @NotNull private String username = "test";
    @NotNull private String password = "test";

    @Before
    public void setUp() throws Exception {
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("projectName", projectName);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));
        @Nullable final String projectResultAsJson = restTemplate.postForObject(PROJECT_CREATE_REST_URL, request, String.class);
        @NotNull final JsonNode root = objectMapper.readTree(projectResultAsJson);
        projectId = root.path("id").asText();

    }

    @Test
    public void findProjectById() throws IOException {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));
        @Nullable final String projectFindResultAsJson = restTemplate.getForObject(PROJECT_FIND_BY_ID_REST_URL + projectId, String.class);
        @NotNull final JsonNode root = objectMapper.readTree(projectFindResultAsJson);
        assertNotNull(projectFindResultAsJson);
        assertNotNull(root);
        assertNotNull(root.path("name").asText());
        assertEquals(root.path("name").asText(), projectName);
        assertNotNull(root.path("id").asText());
        assertEquals(root.path("id").asText(), projectId);
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("id", projectId);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));
        restTemplate.postForObject(PROJECT_DELETE_REST_URL, request, String.class);
    }
}
