package com.morozov.tm.rest.project;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

public class ProjectDeleteRestTest {
    @NotNull private static final String PROJECT_CREATE_REST_URL = "http://localhost:8080/restprojects/create";
    @NotNull private static final String PROJECT_DELETE_REST_URL = "http://localhost:8080/restprojects/delete";
    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();
    @Nullable private String projectId;

    @Before
    public void setUp() throws Exception {
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        @NotNull final String projectName = "Test Project";
        requestBody.add("projectName", projectName);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final String projectResultAsJson = restTemplate.postForObject(PROJECT_CREATE_REST_URL, request, String.class);
        @NotNull final JsonNode root = objectMapper.readTree(projectResultAsJson);
        projectId = root.path("id").asText();
    }

    @Test
    public void deleteProject() {
        @NotNull final HttpHeaders headerDelete = new HttpHeaders();
        headerDelete.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestDeleteBody = new LinkedMultiValueMap<>();
        requestDeleteBody.add("id", projectId);
        @NotNull final HttpEntity<?> requestDelete = new HttpEntity<Object>(requestDeleteBody, headerDelete);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity responseEntity = restTemplate.exchange(PROJECT_DELETE_REST_URL, HttpMethod.POST, requestDelete, String.class);
        @NotNull final HttpStatus status = responseEntity.getStatusCode();
        assertEquals(status, HttpStatus.OK);
    }
}
