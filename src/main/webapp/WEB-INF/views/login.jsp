<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 05.12.2019
  Time: 16:54
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div>
                <h1>Login page</h1>
            </div>
            <c:url value="/login" var="loginUrl"/>
            <form action="${loginUrl}" method="post">
                <c:if test="${param.error != null}">
                    <p>
                        Invalid username and password.
                    </p>
                </c:if>
                <c:if test="${param.logout != null}">
                    <p>
                        You have been logged out.
                    </p>
                </c:if>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="inputUsername"> User name :</label>
                    <div class="col-sm-6">
                        <input class="form-control" type="text" name="username"
                               id="inputUsername" placeholder="User name"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label"> Password: </label>
                    <div class="col-sm-6">
                        <input type="password" name="password" class="form-control"
                               id="inputPassword" placeholder="Password"/>
                    </div>
                </div>
                <input type="hidden"
                       name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Sign in</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="/registration"><h5>Register new user</h5></a>
        </div>
    </div>
</div>
</body>
</html>
