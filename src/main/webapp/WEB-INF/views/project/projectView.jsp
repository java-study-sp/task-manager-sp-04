<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 28.11.2019
  Time: 18:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${project.name}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

<div class="container-fluid">
    <%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
    <div class="row">
        <div class="col">
            <div>
                <h1>Project View</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <div class="list-group">
                <div class="list-group-item list-group-item-action active">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">${project.name}</h5>
                    </div>
                </div>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">ID:</h5>
                    </div>
                    <p class="mb-1">${project.id}</p>
                </div>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Description:</h5>
                    </div>
                    <p class="mb-1">${project.description}</p>
                </div>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Date create:</h5>
                    </div>
                    <p class="mb-1">${project.createdData}</p>
                </div>
                <c:if test="${project.startDate != null}">
                    <div class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Date begin:</h5>
                        </div>
                        <p class="mb-1">${project.startDate}</p>
                    </div>
                </c:if>
                <c:if test="${project.endDate != null}">
                    <div class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Date end:</h5>
                        </div>
                        <p class="mb-1">${project.endDate}</p>
                    </div>
                </c:if>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Status:</h5>
                    </div>
                    <p class="mb-1">${project.status}</p>
                </div>
            </div>
        </div>
        <div class="col">
            <c:choose>
                <c:when test="${!taskList.isEmpty()}">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">№</th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Data Create</th>
                            <th scope="col">Data Begin</th>
                            <th scope="col">Data End</th>
                            <th scope="col">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${taskList}" var="task" varStatus="loop">
                            <tr>
                                <td scope="row">${loop.count}</td>
                                <td scope="row">${task.name}</td>
                                <td scope="row">${task.description}</td>
                                <td scope="row">${task.createdData}</td>
                                <td scope="row">${task.startDate}</td>
                                <td scope="row">${task.endDate}</td>
                                <td scope="row">${task.status}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h3>Task list is empty</h3>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>
