<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 28.11.2019
  Time: 15:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project Edit</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
    <div class="row">
        <div class="col">
            <div class="mt-1 ml-3">
                <h1>Project Edit</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="mt-1 ml-3">
                <form action="/projects/save" method="post">
                    <div class="form-group col-6">
                        <label for="formGroupExampleInput">Project ID</label>
                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Project ID"
                               name="id" value="${project.id}" readonly>
                    </div>
                    <div class="form-group col-6">
                        <label for="formGroupExampleInput1">Project name</label>
                        <input type="text" class="form-control" id="formGroupExampleInput1" placeholder="Project name"
                               name="name" value="${project.name}">
                    </div>
                    <div class="form-group col-6">
                        <label for="formGroupExampleInput2">Project description</label>
                        <input type="text" class="form-control" id="formGroupExampleInput2"
                               placeholder="Project description"
                               name="description" value="${project.description}">
                    </div>
                    <div class="form-group col-6">
                        <label for="formGroupExampleInput3">Project Date Create</label>
                        <input type="text" class="form-control" id="formGroupExampleInput3"
                               placeholder="Project Date Create"
                               name="dataCreate" value="${dataCreate}" readonly>
                    </div>
                    <div class="form-group col-6">
                        <label for="formGroupExampleInput4">Project Date Begin</label>
                        <input type="date" class="form-control" id="formGroupExampleInput4"
                               placeholder="Project Date Begin"
                               name="dataBegin" value="${dataBegin}">
                    </div>
                    <div class="form-group col-6">
                        <label for="formGroupExampleInput5">Project Date End</label>
                        <input type="date" class="form-control" id="formGroupExampleInput5"
                               placeholder="Project Date End"
                               name="dataEnd" value="${dataEnd}">
                    </div>
                    <sec:csrfInput/>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
