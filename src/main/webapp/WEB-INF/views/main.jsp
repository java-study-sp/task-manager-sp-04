<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 28.11.2019
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>MainPage</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>
<div class="container-fluid ">
    <%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
    <div class="row ">
        <div class="col">
            <a href="projects"><h1>Project list</h1></a>
            <a href="tasks"><h1>Task list</h1></a>
        </div>
    </div>
</body>
</html>
