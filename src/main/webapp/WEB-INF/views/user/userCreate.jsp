<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 10.12.2019
  Time: 13:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Registration</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
    <div class="row">
        <div class="col">
            <h1>Register new user</h1>
            <form action="/registration" method="post">

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="inputUsername"> User name :</label>
                    <div class="col-sm-6">
                        <input type="text" name="username" placeholder="Enter new user name" class="form-control"
                               id="inputUsername"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label"> Password: </label>
                    <div class="col-sm-6">
                        <input type="password" name="password" placeholder="Enter user password" class="form-control"
                               id="inputPassword"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label"> Email: </label>
                    <div class="col-sm-6">
                        <input type="email" name="email" placeholder="Enter user email" class="form-control"
                               id="inputEmail"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <sec:csrfInput/>
            </form>
        </div>
    </div>
</body>
</html>
