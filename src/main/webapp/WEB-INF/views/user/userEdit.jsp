<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 10.12.2019
  Time: 13:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
    <div class="row">
        <div class="col">
            <h5>User edit</h5>
            <form action="/users" method="post">
                <div class="form-group">
                    <input type="text" name="username" value="${user.username}" class="form-control"/>
                </div>
                <c:forEach items="${roles}" var="role">
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox"
                                   name="${role}" ${user.roles.contains(role)?"checked":""}/>${role}
                        </div>
                    </div>
                </c:forEach>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                <input type="hidden" name="userId" value="${user.id}"/>
                <sec:csrfInput/>
            </form>
        </div>
    </div>
</body>
</html>
