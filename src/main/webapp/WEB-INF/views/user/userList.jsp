<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 09.12.2019
  Time: 18:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
    <div class="row">
        <div class="col">
            <div>
                <h1>User List</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Roles</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${userlist}" var="user" varStatus="loop">
                    <tr>
                        <td scope="row">${loop.count}</td>
                        <td scope="row">${user.username}</td>
                        <td scope="row">${user.email}</td>
                        <td scope="row">
                            <c:forEach items="${user.roles}" var="role">
                                ${role}
                            </c:forEach>
                        </td>
                        <td scope="row"><a href="/users/edit/${user.id}/">EDIT</a></td>
                        <td scope="row">
                            <form action="/users/delete/${user.id}" method="post">
                                <button type="submit" class="btn btn-danger">REMOVE</button>
                                <sec:csrfInput/>
                            </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <form action="/registration" method="get">
                <button type="submit" class="btn btn-primary">Add User</button>
            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>
