package com.morozov.tm.service;


import com.morozov.tm.api.repository.IUserRepository;
import com.morozov.tm.enumerated.RoleEnum;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.model.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = userRepository.findByUsername(username);
        if (user == null) throw new UsernameNotFoundException("User not found");
        return user;
    }

    public User findById(String id) throws UserNotFoundException {
        @NotNull User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return user;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void updateUser(User user, String password, String email) {
        @Nullable final String userEmail = user.getEmail();
        final boolean isEmailChange = (email != null && !email.equals(userEmail))
                || (userEmail != null && !userEmail.equals(email));
        if (isEmailChange) {
            user.setEmail(email);
        }
        if (!StringUtils.isEmpty(password)) user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    public void saveUser(String username, User user, Map<String, String> form) {
        user.setUsername(username);
        @NotNull final Set<String> collectRoles = Arrays.stream(RoleEnum.values())
                .map(RoleEnum::name)
                .collect(Collectors.toSet());
        user.getRoles().clear();
        for (String key : form.keySet()) {
            if (collectRoles.contains(key)) {
                user.getRoles().add(RoleEnum.valueOf(key));
            }
        }
        userRepository.save(user);
    }

    public boolean addUser(@NotNull final String username,
                           @NotNull final String password,
                           @NotNull final String email) {
        @Nullable final User userDB = userRepository.findByUsername(username);
        if (userDB != null) return false;
        @NotNull final User user = new User();
        user.setUsername(username);
        user.setActive(true);
        user.setRoles(Collections.singleton(RoleEnum.ROLE_USER));
        user.setPassword(passwordEncoder.encode(password));
        user.setEmail(email);
        userRepository.save(user);
        return true;
    }

    public void deleteUser(@NotNull final String id) throws UserNotFoundException {
        @NotNull final User userDB = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        userRepository.delete(userDB);
    }

}
