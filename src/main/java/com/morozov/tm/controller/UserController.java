package com.morozov.tm.controller;

import com.morozov.tm.enumerated.RoleEnum;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.model.entity.User;
import com.morozov.tm.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/users")
public class UserController {
    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "")
    public String userList(Model model) {
        @NotNull final List<User> userList = userService.findAll();
        model.addAttribute("userlist", userList);
        return "user/userList";
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/edit/{id}")
    public String userEditForm(@PathVariable String id, Model model) throws UserNotFoundException {
        @NotNull final User user = userService.findById(id);
        model.addAttribute("user", user);
        List<RoleEnum> roles = Arrays.asList(RoleEnum.values());
        model.addAttribute("roles", roles);
        return "user/userEdit";
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping
    public String userSave(
            @RequestParam String username,
            @RequestParam Map<String, String> form,
            @RequestParam("userId") String id
    ) throws UserNotFoundException {
        @NotNull final User user = userService.findById(id);
        userService.saveUser(username, user, form);
        return "redirect:/users";
    }

    @GetMapping("/profile")
    public String getProfile(Model model,
                             @AuthenticationPrincipal User user) {
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        return "user/userProfile";
    }

    @PostMapping("/profile")
    public String updateUserProfile(@AuthenticationPrincipal User user,
                                    @RequestParam String password,
                                    @RequestParam String email) {
        userService.updateUser(user, password, email);
        return "redirect:/users/profile";
    }

    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") String id) throws UserNotFoundException {
        userService.deleteUser(id);
        return "redirect:/users";
    }
}
