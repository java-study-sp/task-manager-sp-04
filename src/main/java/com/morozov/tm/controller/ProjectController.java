package com.morozov.tm.controller;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.model.dto.ProjectDto;
import com.morozov.tm.model.dto.TaskDto;
import com.morozov.tm.model.entity.User;
import com.morozov.tm.service.UserService;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/projects")
public class ProjectController {
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ITaskService taskService;
    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/all")
    public String projectList(Model model) throws UserNotFoundException {
        @Nullable final List<ProjectDto> projectList = projectService.findAllProject();
        @NotNull final Map<String, String> userNameMap = new HashMap<>();
        for (@NotNull final ProjectDto projectDto : projectList) {
            @NotNull final String userId = projectDto.getUserId();
            @NotNull final String userName = userService.findById(userId).getUsername();
            userNameMap.put(userId,userName);
        }
        model.addAttribute("projectlist", projectList);
        model.addAttribute("userNameMap",userNameMap);
        return "project/projectList";
    }
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String projectListByCurrentUser(
            @AuthenticationPrincipal User user,
            Model model) throws UserNotFoundException {
        @Nullable final List<ProjectDto> projectList = projectService.findAllProjectByUser(user);
        @NotNull final Map<String, String> userNameMap = new HashMap<>();
        for (@NotNull final ProjectDto projectDto : projectList) {
            @NotNull final String userId = projectDto.getUserId();
            @NotNull final String userName = userService.findById(userId).getUsername();
            userNameMap.put(userId,userName);
        }
        model.addAttribute("projectlist", projectList);
        model.addAttribute("userNameMap",userNameMap);
        return "project/projectList";
    }

    @RequestMapping(value = "/view/{projectId}")
    public String projectView(@PathVariable("projectId") String id, Model model) throws ProjectNotFoundException {
        @Nullable final ProjectDto project = projectService.findById(id);
        @NotNull final List<TaskDto> taskList = taskService.findAllTaskByProjectId(id);
        model.addAttribute("project", project);
        model.addAttribute("taskList", taskList);
        return "project/projectView";
    }

    @RequestMapping(value = "/edit/{projectId}")
    public String projectEdit(@PathVariable("projectId") String id, Model model) throws ProjectNotFoundException {
        @Nullable final ProjectDto project = projectService.findById(id);
        @Nullable final String dataBegin = DateFormatUtil.formattedDataToString(project.getStartDate());
        @Nullable final String dataEnd = DateFormatUtil.formattedDataToString(project.getEndDate());
        @Nullable final String dataCreate = DateFormatUtil.formattedDataToString(project.getCreatedData());
        model.addAttribute("project", project);
        model.addAttribute("dataCreate", dataCreate);
        model.addAttribute("dataBegin", dataBegin);
        model.addAttribute("dataEnd", dataEnd);
        return "project/projectEdit";
    }

    @PostMapping(value = "/save")
    public String projectUpdate(
            @RequestParam("id") String projectId,
            @RequestParam("name") String projectName,
            @RequestParam("description") String projectDescription,
            @RequestParam("dataBegin") String dataBegin,
            @RequestParam("dataEnd") String dataEnd
    ) throws ParseException, ProjectNotFoundException {
        projectService.updateProject(projectId, projectName, projectDescription, dataBegin, dataEnd);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/add")
    public String addProject() {
        return "project/projectCreate";
    }

    @PostMapping(value = "/create")
    public String createProject(
            @RequestParam("projectName") String projectName,
            @AuthenticationPrincipal User user
    ) {
        projectService.addProject(projectName, user);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/remove/{projectId}")
    public String removeProject(
            @PathVariable("projectId") String id
    ) throws ProjectNotFoundException {
        projectService.deleteProjectById(id);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/deleteAll")
    public String removeAllProject() {
        projectService.deleteAll();
        return "redirect:/projects";
    }

}
