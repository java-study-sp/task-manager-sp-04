package com.morozov.tm.api.service;

import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.model.dto.ProjectDto;
import com.morozov.tm.model.entity.Project;
import com.morozov.tm.model.entity.User;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    @NotNull
    ProjectDto findById(String id) throws ProjectNotFoundException;

    @NotNull List<ProjectDto> findAllProject();

    @NotNull List<ProjectDto> findAllProjectByUser(@NotNull User user);

    @NotNull
    ProjectDto addProject(@NotNull String projectName, User user);

    void deleteProjectById(@NotNull String id) throws ProjectNotFoundException;

    @NotNull
    ProjectDto updateProject(
            @NotNull String id, @NotNull String projectName, @NotNull String projectDescription, @NotNull String dataStart,
            @NotNull String dataEnd)
            throws ParseException, ProjectNotFoundException;

    @NotNull
    List<ProjectDto> searchByString(@NotNull String string);

    void deleteAll();

    @NotNull
    ProjectDto transferProjectToProjectDto(@NotNull final Project project);

    @NotNull
    Project transferProjectDtoToProject(@NotNull final ProjectDto projectDto);

    @NotNull
    List<ProjectDto> transferListProjectToListProjectDto(@NotNull List<Project> projectList);

}
