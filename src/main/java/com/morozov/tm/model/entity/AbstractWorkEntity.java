package com.morozov.tm.model.entity;

import com.morozov.tm.enumerated.StatusEnum;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;
@MappedSuperclass
public class AbstractWorkEntity extends AbstractEntity {
    @NotNull
    @Column(nullable = false)
    private String name = "";
    @NotNull
    @Column(name = "date_create", nullable = false)
    private Date createdData = new Date();
    @NotNull
    private String description = "";
    @Nullable
    @Column(name = "date_start")
    private Date startDate = null;
    @Nullable
    @Column(name = "date_end")
    private Date endDate = null;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private StatusEnum status = StatusEnum.PLANNED;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    public AbstractWorkEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedData() {
        return createdData;
    }

    public void setCreatedData(Date createdData) {
        this.createdData = createdData;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
