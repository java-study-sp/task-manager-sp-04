package com.morozov.tm.model.dto;

import org.jetbrains.annotations.NotNull;

public class TaskDto extends AbstractWorkEntityDto {
    @NotNull
    private String projectId = "";

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
